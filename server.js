var http = require('http');

var restify = require('restify');
var MongoClient = require('mongodb').MongoClient
    , format = require('util').format;

var server = restify.createServer({
    name: 'faq.me',
    version: '1.0.0'
});

var mongoDbUrl = 'mongodb://127.0.0.1:27017/local';
var mongoCollections = { 'users': 'u', 'questions': 'q', 'answers': 'a' };

server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser());



server.put('/hello', function(req, res, next) {
    //var params = JSON.parse(req.params);
    res.send('hello ' + JSON.stringify(req.params.name));
    return next();
});

server.get('/me', function(req, res, next) {
    //var params = JSON.parse(req.params);
    console.log(req.params);

    MongoClient.connect(mongoDbUrl, function(err, db) {

        if(err) {
            console.log(err);
            throw err;
        }
        var collection = db.collection(mongoCollections.users);
        collection.findOne({id: req.params.id}, function(err, doc) {
            if(err) throw err;
            if(!!doc) {
                //welcome user
                res.setHeader('content-type', 'application/json');
                res.send({isRegistered: true, friends: []});
                console.log('User ' + doc.name + ' has logged in');
            } else {
                res.setHeader('content-type', 'application/json');
                res.send({isRegistered: false});
                console.log('User unregistered');
            }
        });
    });
});

server.put('/me', function(req, res, next) {
    //console.log(JSON.stringify(req))
    //var params = JSON.parse(req.params);
    //console.log(params.name);
    MongoClient.connect(mongoDbUrl, function(err, db) {
        if(err) {
            throw err;
            console.log(err);
        }

        var collection = db.collection(mongoCollections.users);
        collection.findOne({id: req.params.id}, function(err, doc) {
            if(err) throw err;
            if(doc) {
                //welcome user
                res.send('Welcome back');
            } else {
                collection.insert(req.params, function(err, docs) {
                    if(err) throw err;
                    res.send('Thanks for registering.');
                });
            }
        });
    })
    return next();
});

server.get('/users/all', function (req, res, next) {
    MongoClient.connect(mongoDbUrl, function(err, db) {
        if(err) throw err;

        var collection = db.collection('test_insert');
        //collection.insert({a:2}, function(err, docs) {

        collection.count(function(err, count) {
            //console.log(format("count = %s", count));
        });

        // Locate all the entries using find
        collection.find().toArray(function(err, results) {
            console.dir(results);
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.end(JSON.stringify(results))
            // Let's close the db
            db.close();
        });
        //});
    })
    return next();
});

server.get(/.*/, restify.serveStatic({
    'directory': 'static',
    'default': 'home.html'
}));

server.listen(8080, function () {
    console.log('%s listening at %s', server.name, server.url);
});